<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$server = getenv('MYSQL_SERVER');
$db = getenv('MYSQL_DB');
$user = getenv('MYSQL_USER');
$pass = getenv('MYSQL_PASSWORD');

$mysqli = mysqli_connect($server, $user, $pass);

if ($mysqli->connect_errno) {
    echo 'Could not connect to mysql';
    exit;
}

$sql = "SHOW TABLES FROM $db";
$result = $mysqli->query($sql);

if (!$result) {
    echo "DB Error, could not list tables\n";
    echo 'MySQL Error: ' . mysql_error();
    exit;
}

$tables = array("<strong>$db - Tables</strong>");

while ($row = $result->fetch_object()){
        $table = (array)$row;
        $tables[] = array_shift($table);        
}

echo implode($tables, "<br>");

mysqli_free_result($result);
?>
